
const mongoose  = require("mongoose");
let orderSchema = new mongoose.Schema({
	totalAmount: {type:Number},
	purchasedOn: {
		type:Date,
		default: new Date()
	},
	userId     : {type:String},
	products   : [{
		productId:{type:String},
		quantity : {type:Number},
		price    : {type:Number}
	}]
})

module.exports = mongoose.model("Order",orderSchema);




